/**
 * @file
 * Commerce Twenga Feed Google Shopping javascript file.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.commerceTwengaFeedGoogleShoppingLink = {
    attach: function (context, settings) {
      $('#tw-form-signup .button-wrap').once(function () {
        $('#no-account').insertAfter('#tw-form-signup-submit');
      });
      $('#tw-form-login .button-wrap').once(function () {
        $('#no-account').insertAfter('#tw-form-login-submit');
      });
      $('a[data-target="#tw-form-lostpassword"]').attr('href', 'password_lost');
    }
  };
})(jQuery);
