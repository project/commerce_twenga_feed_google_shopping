<?php

/**
 * @file
 * Commerce Twenga Feed Google Shopping module file.
 */

define('COMMERCE_TWENGA_FEED_GOOGLE_SHOPPING_API_URL', 'https://api.twenga-solutions.com/');
define('COMMERCE_TWENGA_FEED_GOOGLE_SHOPPING_USER_AGENT', 'drupal');
define('COMMERCE_TWENGA_FEED_GOOGLE_SHOPPING_DEFAULT_LANG', 'UK');
define('COMMERCE_TWENGA_FEED_GOOGLE_SHOPPING_DEFAULT_REGION', 'content');

/**
 * Internal function.
 *
 * @param string $path
 *   Path.
 * @param array $params
 *   Parameters.
 * @param string $method
 *   Method HTTP.
 * 
 * @return mixed
 *   Webservice result.
 */
function _commerce_twenga_feed_google_shopping_request($path, $params = array(), $method = 'GET') {
  // Default get params.
  $get_params = array(
    'GEOZONE_CODE' => _commerce_twenga_feed_google_shopping_set_api_language(),
    'PRODUCT_ID' => 7,
  );

  // Default options.
  $options = array(
    'headers' => array(
      'Content-Type' => 'application/x-www-form-urlencoded',
      'User-Agent' => COMMERCE_TWENGA_FEED_GOOGLE_SHOPPING_USER_AGENT,
    ),
    'method' => $method,
    'data' => NULL,
  );

  // Extend default params if is requested.
  if (!empty($params)) {
    // If request is authenticate format params for HTTP Basic Auth.
    if ($path == 'authenticate') {
      $login_info = 'Basic ' . base64_encode($params['user'] . ':' . $params['password']);
      $options['headers']['Authorization'] = $login_info;
      $get_params = array();
    }
    else {
      // If method is GET then merge requested params.
      if ($method == 'GET') {
        $get_params = $params;
      }
      // If method is POST then insert query params on options data array.
      if ($method == 'POST') {
        $options['data'] = drupal_http_build_query($params);
      }
    }
  }

  // Build query if get params are not empty.
  if (!empty($get_params)) {
    $get_params = drupal_http_build_query($get_params);
    $url = COMMERCE_TWENGA_FEED_GOOGLE_SHOPPING_API_URL . $path . '?' . $get_params;
  }
  else {
    $url = COMMERCE_TWENGA_FEED_GOOGLE_SHOPPING_API_URL . $path;
  }

  // Do the request.
  $request = drupal_http_request($url, $options);
  // Collect response data.
  $data = $request->data;
  // Format response data.
  $result = json_decode($data, TRUE);

  return $result;
}

/**
 * @return mixed|string
 */
function _commerce_twenga_feed_google_shopping_set_api_language() {
  global $language;

  // Array of Twenga lang available.
  $twenga_lang = array(
    'FR',
    'UK',
    'NL',
    'DE',
    'IT',
    'ES',
    'PL',
  );

  // Get current Drupal lang.
  $drupal_lang = strtoupper(substr($language->language, 0, 2));
  // Check if Drupal lang is available on Twenga.
  $api_lang = array_search($drupal_lang, $twenga_lang);
  // If available set Twenga lang else set default Twenga lang.
  $api_lang = is_numeric($api_lang) ? $twenga_lang[$api_lang] : COMMERCE_TWENGA_FEED_GOOGLE_SHOPPING_DEFAULT_LANG;

  return $api_lang;
}

/**
 * @return mixed
 */
function _commerce_twenga_feed_google_shopping_get_login_form() {
  // Get Twenga login form.
  $login_form = _commerce_twenga_feed_google_shopping_request('module/login');

  return $login_form;
}

/**
 * @return mixed
 */
function _commerce_twenga_feed_google_shopping_get_signup_form() {
  // Get Twenga signup form.
  $signup_form = _commerce_twenga_feed_google_shopping_request('module/signup');

  return $signup_form;
}

/**
 * @return mixed
 */
function _commerce_twenga_feed_google_shopping_signup() {
  // Process the signup request.
  $result = _commerce_twenga_feed_google_shopping_request('module/signup', $_REQUEST, 'POST');

  if (!empty($result['errors'])) {
    foreach ($result['errors'] as $key => $value) {
      drupal_set_message($key . ' ' . $value, 'error');
    }
  }
  else {
    variable_set('commerce_twenga_feed_google_shopping_user_info', $result);
    _commerce_twenga_feed_google_shopping_auto_login();
    _commerce_twenga_feed_google_shopping_setup_tracker();
  }

  drupal_goto('admin/commerce/config/commerce_twenga_feed_google_shopping');
}


/**
 * @return mixed
 */
function _commerce_twenga_feed_google_shopping_login() {
  // Process the login request.
  $result = _commerce_twenga_feed_google_shopping_request('authenticate/email', $_POST, 'POST');

  if ($result !== NULL) {
    if (!empty($result['errors'])) {
      foreach ($result['errors'] as $key => $value) {
        drupal_set_message($key . ' ' . $value, 'error');
      }
    }
    else {
      variable_set('commerce_twenga_feed_google_shopping_user_info', $result);
      _commerce_twenga_feed_google_shopping_setup_tracker();
    }
  }

  drupal_goto('admin/commerce/config/commerce_twenga_feed_google_shopping');
}

/**
 * @return mixed
 */
function _commerce_twenga_feed_google_shopping_auto_login() {
  // Process the auto login request.
  $login_info = variable_get('commerce_twenga_feed_google_shopping_user_info', NULL);

  if ($login_info !== NULL) {
    $params = array(
      'user' => $login_info['merchant']['EXTRANET_SITE_ID'],
      'password' => $login_info['merchant']['API_KEY'],
    );

    $user_info = _commerce_twenga_feed_google_shopping_request('authenticate', $params);
    variable_set('commerce_twenga_feed_google_shopping_user_info', $user_info);
    _commerce_twenga_feed_google_shopping_setup_tracker();
  }
  else {
    drupal_goto('admin/commerce/config/commerce_twenga_feed_google_shopping');
  }
}

/**
 * Internal function to retrieve password.
 *
 * @param array $params
 *   Parameters.
 */
function _commerce_twenga_feed_google_shopping_retrieve_password($params) {
  // Process the retrieve password request.
  $result = _commerce_twenga_feed_google_shopping_request('/module/lostpassword', $params, 'POST');

  if ($result !== NULL) {
    if (!empty($result['errors'])) {
      foreach ($result['errors'] as $key => $value) {
        drupal_set_message($key . ' ' . $value, 'error');
      }
    }
    else {
      if (!empty($result['success'])) {
        foreach ($result['success'] as $key => $value) {
          drupal_set_message($key . ' ' . $value, 'status');
        }
      }
    }
  }

  drupal_goto('admin/commerce/config/commerce_twenga_feed_google_shopping');
}

/**
 * Internal function to get products information.
 */
function _commerce_twenga_feed_google_shopping_get_products_info() {
  // Get Twenga products information.
  $user_info = variable_get('commerce_twenga_feed_google_shopping_user_info', NULL);

  if (!empty($user_info['auth']['token'])) {
    $params = array(
      'token' => $user_info['auth']['token'],
    );
    $result = _commerce_twenga_feed_google_shopping_request('product', $params);

    if ($result !== NULL) {
      if (!empty($result['error'])) {
        drupal_set_message($result['error'], 'error');
      }
      else {
        $products = array();

        foreach ($result['products'] as $product) {
          $products[$product['PRODUCT_ID']] = $product['EXTRANET_STATUS'];
        }

        variable_set('commerce_twenga_feed_google_shopping_products_info', $products);
      }
    }
  }
}

/**
 * Internal function to store Twenga tracker.
 */
function _commerce_twenga_feed_google_shopping_setup_tracker() {
  // Save the Twenga tracker.
  $user_info = variable_get('commerce_twenga_feed_google_shopping_user_info', NULL);

  if ($user_info['auth']['token'] !== NULL) {
    $token = array('token' => $user_info['auth']['token']);
    $tracker = _commerce_twenga_feed_google_shopping_request('tracker', $token);

    if (!empty($tracker['error'])) {
      foreach ($tracker['error'] as $key => $value) {
        drupal_set_message($key . ' ' . $value);
      }
    }
    else {
      variable_set('commerce_twenga_feed_google_shopping_tracker', $tracker);
    }
  }
}
