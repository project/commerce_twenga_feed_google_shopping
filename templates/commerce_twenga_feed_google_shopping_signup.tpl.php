<?php

/**
 * @file
 * Commerce Twenga Feed Google password lost template.
 */
?>
<a href="https://www.twenga-solutions.com/fr/smartsem/smartfeed/?utm_medium=partner&utm_campaign=module_drupal_smartfeed&utm_source=drupal&utm_content=bo">
  <img src="<?php print base_path() . drupal_get_path('module', 'commerce_twenga_feed_google_shopping');?>/images/header-twenga.png" alt="Twenga" />
</a>
<h3 class="main-title"><?php print t('Unlock your Google Shopping performance with the most powerful automation solution.'); ?></h3>

<div class="tw-form" id="tw-form-signup">
  <h4 class="title active"><?php print t('Step 1: Create your account'); ?></h4>
  <?php print $form['html']; ?>
  <?php print l(t('I already have a Twenga Solutions account'), base_path() . 'admin/commerce/config/commerce_twenga_feed_google_shopping/login', array('attributes' => array('id' => 'no-account'))); ?>
</div>

<div class="tw-form" id="tw-form-next">
  <h4 class="title inactive"><?php print t('Step 2: Finalise your Twenga Solutions module installation'); ?></h4>
</div>
