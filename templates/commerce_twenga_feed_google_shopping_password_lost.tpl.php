<?php

/**
 * @file
 * Commerce Twenga Feed Google password lost template.
 */
?>
<a href="https://www.twenga-solutions.com/fr/smartsem/smartfeed/?utm_medium=partner&utm_campaign=module_drupal_smartfeed&utm_source=drupal&utm_content=bo">
  <img src="<?php print base_path() . drupal_get_path('module', 'commerce_twenga_feed_google_shopping');?>/images/header-twenga.png" alt="Twenga" />
</a>
<h3 class="main-title"><?php print t('Unlock your Google Shopping performance with the most powerful automation solution.'); ?></h3>

<div class="tw-form" id="tw-form-password-lost">
  <?php print drupal_render($form); ?>
</div>
