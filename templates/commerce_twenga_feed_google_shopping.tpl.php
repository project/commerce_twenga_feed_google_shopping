<?php

/**
 * @file
 * Commerce Twenga Feed Google admin template.
 */

$module_path = drupal_get_path('module', 'commerce_twenga_feed_google_shopping');
$products_info = variable_get('commerce_twenga_feed_google_shopping_products_info', NULL);
$user_info = variable_get('commerce_twenga_feed_google_shopping_user_info', NULL);
$url_dashboard = $user_info['user']['AUTO_LOG_URL'] . '&utm_medium=partner&utm_campaign=module_drupal_smartfeed&utm_source=drupal&utm_content=bo';
$tracker = variable_get('commerce_twenga_feed_google_shopping_tracker', NULL);

?>
<a href="https://www.twenga-solutions.com/fr/smartsem/smartfeed/?utm_medium=partner&utm_campaign=module_drupal_smartfeed&utm_source=drupal&utm_content=bo">
  <img src="<?php print base_path() . drupal_get_path('module', 'commerce_twenga_feed_google_shopping'); ?>/images/header-twenga.png" alt="Twenga" />
</a>
<h3 class="main-title"><?php print t('Unlock your Google Shopping performance with the most powerful automation solution.') ?></h3>

<div class="tw-form" id="tw-form-login">
  <h4 class="title success"><?php print t('Step 1: Configure your account')?></h4>
</div>

<div class="tw-form" id="tw-form-access">
  <h4 class="title active"><?php print t('Step 2: Finalise your Twenga Solutions module installation'); ?></h4>

  <?php if (!empty($products_info) || $products_info[7]['EXTRANET_STATUS'] === 'COMPLETED') : ?>
    <?php print t('Congratulations you have now installed Twenga Tracking!'); ?>

    <p><?php t('With Twenga Tracking:')?></p>

    <ul class="tw-list">
      <li><?php print t('I can measure the quality of my traffic by following my conversion rates and acquisition costs per category.'); ?></li>
      <li><?php print t("I can optimise my budget by prioritising the highest performing offers thanks to Twenga's automatic settings."); ?></li>
      <li><?php print t('I can secure my performance thanks to proactive monitoring and recommendations from the Twenga teams.'); ?></li>
    </ul>
    <?php print l(t('Continue to your interface'), $url_dashboard, array('attributes' => array('class' => 'btn', 'id' => 'tw-account'))); ?>
  <?php else: ?>
    <div class="tw-alert"><?php print t('Warning: We have now taken your request into account. In order to benefit from our services you must finalise your subscription.'); ?></div>
    <?php print l(t('Finalise your subscription'), $url_dashboard, array('attributes' => array('class' => 'btn', 'id' => 'finalize-account'))); ?>
  <?php endif ?>
</div>
